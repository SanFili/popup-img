import React from 'react';
import PropTypes from 'prop-types';
import style from './Button.module.scss';
import cn from 'classnames';

const Button = ({ label, classButton, onClick }) => {
 return (
  <button className={cn(style.button, classButton)} onClick={onClick}>
   {label}
  </button>
 );
};

Button.propTypes = {
 label: PropTypes.string.isRequired,
};

export default Button;
