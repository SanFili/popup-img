import React, { useRef, useState } from 'react';
import style from './Popup.module.scss';
import cn from 'classnames';
import { useEffect } from 'react';
import { images } from './data';

const Popup = ({ open, setOpen }) => {
 const [selected, setSelected] = useState(1);
 const [full, setFull] = useState(false);
 const [zoom, setZoom] = useState(false);
 const [showBlocks, setShowBlocks] = useState(true);
 const popupRef = useRef(null);
 const [changeImgAnim, setChangeImgAnim] = useState(false);

 useEffect(() => {
  const closePopup = (e) => {
   if (
    open &&
    e.target !== popupRef.current &&
    !popupRef.current.contains(e.target)
   ) {
    setOpen(false);
   }
  };
  document.addEventListener('click', closePopup);
  return () => document.removeEventListener('click', closePopup);
 }, [open, setOpen]);

 useEffect(() => {
  if (full) setShowBlocks(true);
  else setShowBlocks(false);
 }, [full]);

 return (
  <div
   className={cn(
    style.popup,
    open && style.popup_open,
    full && style.popup_full
   )}
   ref={popupRef}
  >
   <div className={style.popup__container}>
    <div
     className={cn(style.popup__main, changeImgAnim && style.popup__main_anim)}
    >
     <div className={style.popup__photoContainer}>
      <img
       className={cn(
        style.popup__photo,
        zoom && style.popup__photo_zoom,
        full && style.popup__photo_full
       )}
       src={images[selected].img}
       alt=""
      />
     </div>

     {!full && (
      <div className={style.popup__description}>
       <h2 className={style.popup__name}>{images[selected].title}</h2>
       <p className={style.popup__about}>{images[selected].description}</p>
      </div>
     )}
    </div>
    <div className={style.popup__panel}>
     <button className={style.popup__button} onClick={() => setOpen(false)}>
      <img className={style.popup__icon} src="./icons/close.svg" alt="x" />
     </button>
     <button
      className={style.popup__button}
      onClick={() => setFull((prev) => !prev)}
     >
      <img
       className={style.popup__icon}
       src={full ? './icons/fullOff.svg' : './icons/fullOn.svg'}
       alt="open"
      />
     </button>
     <button
      className={style.popup__button}
      onClick={() => setZoom((prev) => !prev)}
     >
      <img
       className={style.popup__icon}
       src={zoom ? './icons/zoomOut.svg' : './icons/zoomIn.svg'}
       alt="zoom"
      />
     </button>
     <button
      className={style.popup__button}
      onClick={() => setShowBlocks((prev) => !prev)}
     >
      <img
       className={style.popup__icon}
       src="./icons/blocks.svg"
       alt="blocks"
      />
     </button>
     <button className={style.popup__button}>
      {selected + 1}/{images.length}
     </button>
    </div>
    <div
     className={cn(
      style.popup__minisList,
      showBlocks && style.popup__minisList_hidden
     )}
    >
     {images.map((image, i) => (
      <img
       key={image.img}
       className={style.popup__miniature}
       src={image.img}
       alt=""
       onClick={() => {
        setSelected(i);
        setChangeImgAnim(true);
        setTimeout(() => setChangeImgAnim(false), 1000);
       }}
      />
     ))}
    </div>
   </div>
  </div>
 );
};

export default Popup;
