import React from 'react';

import Popup from '../components/Popup/Popup';

export default {
 title: 'Example/Popup',
 component: Popup,
};

const Template = (args) => <Popup {...args} />;

export const Default = Template.bind({});
