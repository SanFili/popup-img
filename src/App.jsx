import { useState } from 'react';
import './App.css';
import Popup from './components/Popup/Popup';
import Button from './components/Button/Button';

function App() {
 const [openPopup, setOpenPopup] = useState(false);

 return (
  <div className="App">
   <Button onClick={() => setOpenPopup(true)} label="Открыть" />
   <Popup open={openPopup} setOpen={setOpenPopup} />
  </div>
 );
}

export default App;
